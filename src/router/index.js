import Vue from 'vue'
import Router from 'vue-router'
import Buckets from '@/components/Buckets'
import PriceTable from '@/components/PriceTable'
import PriceTable2 from '@/components/PriceTable2'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Buckets',
      component: Buckets
    },
    {
      path: '/priceTable',
      name: 'PriceTable',
      component: PriceTable
    },
    {
      path: '/priceTable2',
      name: 'PriceTable2',
      component: PriceTable2
    }
  ]
})
